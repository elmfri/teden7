# Naloge v sedmem tednu Elm@FRI


## Naloga 1
Nalogo iz prejšnjega tedna (LightOut) nadgradite tako, da bodo ob zagonu lučke naključno prižgane.

## Naloga 2.
Implementirajte aplikacijo, ki uporabniku postavlja vprašanja in preverja njegove odgvore.
Trenutno stanje kviza je povzeto z spodnjim modelom.



```elm
type alias Question =
    String


type alias Answer =
    String


type alias Model =
    { question : Question
    , answer : Answer
    , currAnswer : Answer
    , checked : Bool
    , correct : Bool
    , countCorrect : Int
    }
```

 V nekem trenutku imamo:
1. podano vprašanje (question),
2. pravilen odgovor na to vprašanje (answer),
3. trenutno vpisan odgovor na vprašanje (currAnswer),
4. ali je bil ta odgovor že preverjen (checked),
5. ali je ta odgovor pravilen in števec pravilnih odgovorov (countCorrect). 

Zbirko vprašanj in odgovorov imate pripravljeno v modulu Questions, kot seznam parov `(Question, Answer)`.  Vsa vprašanja imajo enobesedne odgovore, odgovor pa štejte za pravilen, če je povsem enak privzetemu odgovoru, razlikuje se lahko zgolj v malih (velikih) črkah.
Aplikacija naj naključno izbira med vprašanji, uporabniku pa naj da na voljo samo eno možnost za preverjanje odgvorora.

Enostaven primer igranja tega kviza si lahko ogledate na spodnji gibljivi sliki.

![](example.gif)
