module Questions exposing (..)


trivia =
    [ ( "In 1945 Japanese forces in the ---------- surrender to Allies.", "philippines" )
    , ( "The first telephone call was made in what year", "1876" )
    , ( "What word links these: comic, singer, soap", "opera" )
    , ( "Space indiana jones: what did drinking from the grail 'grant'", "immortality" )
    , ( "TV & Movies: Marx Movies: Whatever it is I am against it", "horsefeathers" )
    , ( "What shakespearean king was actually king of scotland for 17 years", "macbeth" )
    , ( "This raccoon uses his stealth, cleverness, and sometimes lying to complete his missions, which often involve stealing for a good cause.", "sly" )
    , ( "What is the Capital of: Brazil", "brasilia" )
    , ( "Supposed paranormal force moving objects at a distance", "telekinesis" )
    , ( "Baby Names Beginning With 'T': Meaning: A Fairy Queen", "tania" )
    , ( "Category: Trivia : Who was Ancient Egyptian fertility god", "min" )
    , ( "Famous Last Words: You wouldn't hit a guy with -------- on, would you.", "glasses" )
    , ( "TV & Movies: 1997, This Movie was Released on June 6 ----------", "bliss" )
    , ( "In what year did the author Daniel Defoe die", "1731" )
    , ( "Scoleciphobia is the fear of", "worms" )
    , ( "How many stars can be collected in \" Super Mario 64 DS \"?", "150" )
    , ( "Baby Names Beginning With 'K': Meaning: Perfect", "kamil" )
    , ( "Vientiane is the capital of ______", "laos" )
    , ( "TV & Movies: Moronic Duo 1: B&B called a 1-900 phone sex line and listened to a woman's all night.", "butt" )
    , ( "Baby Names Beginning With 'M': Meaning: Lucky", "maimun" )
    , ( "TV & Movies: Category: ActPersons: The birthplace (city) of Jack Lemmon.", "boston" )
    , ( "In 1908 Buddy---------- , actor (Beverly Hillbillies, Barnaby Jones), born.", "ebsen" )
    , ( "In 1956 Carrie ---------- (in Beverly Hills), actor (Star Wars, Blues Brothers), born.", "fisher" )
    , ( "Category: Trivia : In ancient Egypt which animal was considered sacred", "cat" )
    , ( "Visual representation of individual people, distinguished by references to the subject's character, social position, wealth, or profession.", "portraiture" )
    , ( "What subject did Mr. Chips teach", "latin" )
    , ( "Category: Sport : How many games must you win to win a normal set in tennis", "six" )
    , ( "Quotations: 'The human race has one really effective weapon, and that is ---------.'- Mark Twain (1835 - 1910)", "laughter" )
    , ( "What releases an explosive charge of air that moves at speeds up to 60 mph", "cough" )
    , ( "UnScramble this Word: o i s r e p n r s", "prisoners" )
    , ( "Name The Year: Roald Amundsen, Norwegian explorer, discoverer of South Pole", "1872" )
    , ( "Baby Names Beginning With 'M': Meaning: Righteous Way", "michi" )
    , ( "In which country are 'fajitas' a traditional dish", "mexico" )
    , ( "In the Christian calendar, what is the alternative name for the Feast of Pentecost", "whitsun" )
    , ( "Category: Medicine : A bone specialist is a(n) ________.", "osteopath" )
    , ( "In 1824 Kapiolani defies ---------- (Hawaiian volcano goddess) and lives.", "pele" )
    , ( "Category: Trivia : Jonquil is a shade of which colour", "yellow" )
    , ( "In Greek mythology, who was the first woman on Earth, created by Hephaestus at the request of Zeus", "pandora" )
    , ( "What are the only two london boroughs that start with the letter 'e'", "ealing" )
    , ( "In what year did Alaska become the 49th state of America", "1959" )
    , ( "The left lung is smaller than the right lung to make room for what", "heart" )
    , ( "Jimmy Carter once thought he saw a UFO; what was it", "venus" )
    , ( "Which country is the biggest consumer of wine", "france" )
    , ( "Two short words are combined to give the name of which small stand with several shelves or layers for displaying ornaments", "whatnot" )
    , ( "What drug is obtained from the poppy plant", "opium" )
    , ( "What instrument do doctors usually have around their necks", "stethoscope" )
    , ( "Category: Books: Wrote The Teachings of Don Juan: A Yaqui Way of Knowledge.", "castaneda" )
    , ( "In the 1938 film 'Bringing Up Baby', what was Baby", "leopard" )
    , ( "Category: Alcohol: Monastic order that established the California wine industry", "franciscan" )
    , ( "A sailor who has not yet crossed the equator is referred to by what name", "pollywog" )
    , ( "If you were born on 24 February what star sign (Zodiac) would you be", "pisces" )
    , ( "TV & Movies: Category: Commitments: In what year was this film released", "1991" )
    , ( "the two rival gangs in 'west side story' were the sharks and the _________?", "jets" )
    , ( "Sydney 2000 Olympics: This countries medal tally was: 0 Gold, 1 Silver, 0 Bronze, 1 in Total", "uruguay" )
    , ( "What dog shares his owner with Garfiled the Cat?", "odie" )
    , ( "Category: Biology : Every human has one of these on their tummies.", "navel" )
    , ( "Baby Names Beginning With 'N': Meaning: Son of NEAL", "nelson" )
    , ( "Names what portable object is the teleram t-3000", "computer" )
    , ( "Saturday is named for which planet", "saturn" )
    , ( "Useless Trivia: Hairstylist Anthony Silvestri cuts hair while---------- .", "underwater" )
    , ( "Pediophobia is the fear of", "dolls" )
    , ( "In 1708 ---------- von Haller, the father of experimental physiology", "albrecht" )
    , ( "UnScramble this Word: n r e g i r a", "rangier" )
    , ( "Useless Trivia: Human ---------- is estimated to grow at 0.00000001 miles per hour.", "hair" )
    , ( "What northern country Helsinki the capital of", "finland" )
    , ( "Animal Trivia: A male ---------- that has been neutered is known as a 'wether.'", "goat" )
    , ( "Long necked long legged wading bird", "heron" )
    , ( "UnScramble this Word: n l b r e i", "berlin" )
    , ( "TV & Movies: 1996 69th Academy Awards: Best Actress In A Leading Role Was won by Frances Mcdormand For The Movie:", "fargo" )
    , ( "Name The Year: Carl Lewis runs 100m in 9.86 seconds.", "1991" )
    , ( "In 1935 ---------- and Harriet Nelson married.", "ozzie" )
    , ( "Super glue is used to lift fingerprints from what surfaces", "difficult" )
    , ( "Football the seattle ________", "seahawks" )
    , ( "UnScramble this Word: v s u s e r", "versus" )
    , ( "What is the name given to the fortified gateway of a castle", "barbican" )
    , ( "Name one of the countries to join the Commonwealth in 1995.cameroon", "mozambique" )
    , ( "Name The Year: US actress Grace Kelly marries Monaco's Prince Rainier III", "1956" )
    , ( "Name The Year: First man-powered flight (Bryan Allen in Gossamer Condor).", "1977" )
    , ( "Which is the only English word to both begin and end with the letters U-N-D ?", "underground" )
    , ( "Who wrote most of the new testament books", "paul" )
    , ( "What is the largest city in Texas", "houston" )
    , ( "In what year was insulin first used to treat diabetes", "1922" )
    , ( "What's the only property an orthodox Hindu woman can own?", "jewelry" )
    , ( "TV & Movies: Category: Movie Trivia: Movie that featured the line 'Here's looking at you kid.'", "casablanca" )
    , ( "Category: Misc Games: Word derived from 'shah mat', from the arabic for 'the king is dead'", "checkmate" )
    , ( "Who is the Greek messenger god", "hermes" )
    , ( "What is the study of mankind called", "anthropology" )
    , ( "Which acid builds up in the body during excessive exercise", "lactic" )
    , ( "Phthiriophobia is the fear of", "lice" )
    , ( "Category: Clive Barker: To the Seerkind, normal people are known as _______ (Weaveworld)", "cuckoos" )
    , ( "In 1520 Martin ---------- publicly burned papal edict demanding that he recant.", "luther" )
    , ( "Name The Year: Federal Bureau of Investigation established.", "1908" )
    , ( "80s Films: First ___", "blood" )
    , ( "What game challenges you to 'double in' & 'double out'", "darts" )
    , ( "In 1087 William I The Conqueror, King of England and Duke of---------- , dies.", "normandy" )
    , ( "What major city is served by Gatwick Airport", "london" )
    , ( "Name The Year: Phil Ochs rock producer, dies.", "1976" )
    , ( "Soceraphobia is the fear of", "parents-in-law" )
    , ( "In 1892, who raised the marriageable age for girls to 12 years old", "italy" )
    , ( "What was the former German name of the Czech town of Ceske Budejovice", "budweis" )
    , ( "Baby Names Beginning With 'E': Meaning: Greek Mythological Figure", "eurydice" )
    , ( "UnScramble this Word: o t h u g b r", "brought" )
    , ( "Useless Trivia: Mongooses were brought to Hawai'i to kill rats. This plan failed because rats are ---------- while the mongoose hunts during the day.", "nocturnal" )
    , ( "Animal Trivia: There are about 40 different ---------- in a birds wing.", "muscles" )
    , ( "What is a group of this animal called: Dove", "dule" )
    , ( "Whose hamburger patties weigh 1.6 oz", "mcdonald's" )
    , ( "Brothers A terrapin is a type of _________.", "turtle" )
    , ( "If you were born on 26 March what star sign (Zodiac) would you be", "aries" )
    , ( "Baby Names Beginning With 'Y': Meaning: Quiet", "yoshi" )
    , ( "What is the flower that stands for: bonds", "convolvulvus" )
    , ( "80s Films: Dirty ___", "dancing" )
    , ( "In which country would you find the Pripyet Marshes", "belarus" )
    , ( "Baby Names Beginning With 'J': Meaning: Jade", "jadzia" )
    , ( "Which substance, occurring naturally in fruit, causes jams and preserves to set", "pectin" )
    , ( "Baby Names Beginning With 'M': Meaning: Wished-For Child", "mariam" )
    , ( "Whose patron is St Francis de Sales", "teachers" )
    , ( "Category: Trivia : What is the state capital of Florida", "tallahassee" )
    , ( "Category: Ad Slogans: 'You will'", "att" )
    , ( "Who ruled the seas in Greek mythology", "poseidon" )
    , ( "Into what ocean does the Zambezi river flow", "indian" )
    , ( "UnScramble this Word: a r k o e", "korea" )
    , ( "UnScramble this Word: a k i d n m n", "mankind" )
    , ( "What Italian city is considered the fashion capital?", "milan" )
    , ( "What is the flower that stands for: boldness", "pink" )
    , ( "Mechanophobia is the fear of", "machines" )
    , ( "In 1969 Dr. Denton ---------- implants first temporary artificial heart.", "cooley" )
    , ( "Aussie Slang: Oldies", "parents" )
    , ( "Time ____ when your having fun", "flies" )
    , ( "Name The Year: F.B.I. begins it's '10 most wanted list'.", "1950" )
    , ( "TV & Movies: Who was Clark Kent", "superman" )
    , ( "TV & Movies: Category: Famous Celebrities: What is the surname of 8-times married actress Zsa Zsa", "gabor" )
    , ( "Category: Geography : What is the capital of Niger", "niamey" )
    , ( "Who painted 'The Naked Maja'", "goya" )
    , ( "Category: General : At one time, 6 white beads of this Indian currency were worth one penny", "wampum" )
    , ( "What is a group of grouse", "pack" )
    , ( "What is a male sheep", "ram" )
    , ( "Baby Names Beginning With 'S': Meaning: Valuable", "sterling" )
    , ( "In italy, as what is mickey mouse known", "topolino" )
    , ( "TV & Movies: Category: Anime: What is the other (not Japanese) nationality of Asuka Soryuu Langley", "german" )
    , ( "Animal Trivia: From crocodile farms, Australia exports about 5,000 crocodile skins a year. Most go to Paris, where a crocodile purse can sell for more than ----------", "$10" )
    , ( "What indian tribe is associated with 'the trail of tears'", "cherokee" )
    , ( "TV & Movies: 1999, This Movie was Released on August 6 Mystery ----------", "men" )
    , ( "What arabian peninsula nations recently merged under communist leadership", "yemen" )
    , ( "UnScramble this Word: d e s e d", "deeds" )
    , ( "TV & Movies: Category: Highlander: What was the name of Duncan MacLeod's monk mentor?", "darius" )
    , ( "In 1967 Che Guevara executed in---------- .", "bolivia" )
    , ( "Air is 21% oxygen, 78% ______, and 1% other gases", "nitrogen" )
    , ( "What type of number describes the ratio of the speed of a plane to the speed of sound", "mach" )
    , ( "UnScramble this Word: t d o m i e h", "ethmoid" )
    , ( "Animal Trivia: While many people believe that a camel's humps are used for water storage, they are actually made up of ----------. The hump of a well-rested, well-fed camel can weigh up to eighty pounds.", "fat" )
    , ( "If you were born on 26 October what star sign (Zodiac) would you be", "scorpio" )
    , ( "Name The Year: Sir Walter Scott, Scottish novelist, poet (Lady of Lake, Ivanhoe), born.", "1771" )
    , ( "TV & Movies: Category: Star Trek Deep Space 9: Name of the trill inside Jadzia", "dax" )
    , ( "Baby Names Beginning With 'V': Meaning: Health or Love", "valentina" )
    , ( "Year in which the Battle of Balaklava took place", "1854" )
    , ( "Condition in a circuit in which the combined impedances of the capacity and induction to alternating currents cancel each other out or reinforce each other?", "resonance" )
    , ( "What struck honshu island, japan in 1934 killing 4,000 people", "typhoon" )
    , ( "Who was the second king of israel", "david" )
    , ( "Category: Trivia : When was the Rosetta stone found", "1799" )
    , ( "UnScramble this Word: g a i r h e", "hegira" )
    , ( "Useless Trivia: It is illegal to hunt ---------- in the state of Arizona.", "camels" )
    , ( "Baby Names Beginning With 'C': Meaning: Maiden", "corinna" )
    , ( "A male singer whose sexual organs have been modified is known as a what", "castrato" )
    , ( "In video games, what is the name of Sonic the Hedgehog’s sidekick?", "tails" )
    , ( "US Captials - Maine", "augusta" )
    , ( "Baby Names Beginning With 'H': Meaning: Rabbit", "hazeka" )
    , ( "Category: History : What was the instrument of execution during the 'Reign of Terror'", "guillotine" )
    , ( "In 1942 Japanese occupied", "manila" )
    , ( "In 1872 Emily ---------- authority on social behavior, writer (Etiquette), born.", "post" )
    , ( "TV & Movies: 1978 - Anthony Hopkins - Starred In This Movie:", "magic" )
    , ( "A bone specialist is a________", "osteopath" )
    , ( "UnScramble this Word: k a b l c", "black" )
    , ( "Category: Geography : What canal connects Lake Ontario and Lake Erie", "welland" )
    , ( "What is a male swine called (giggle no ex boyfriends names...)", "boar" )
    , ( "Creation of programs, databases etc.for computer applications", "authoring" )
    , ( "What nationality is the keyboards wizard Vangelis", "greek" )
    , ( "In the abbreviation VDU what does the V stand for", "visual" )
    , ( "The 'purple heart' medal was created in 1668, 1701 or 1782", "1782" )
    , ( "In 1066 Battle of Hastings, in which William the ---------- wins England.", "conqueror" )
    , ( "In which year this century were there 3 Popes", "1978" )
    , ( "Baby Names Beginning With 'C': Meaning: Strong Man", "cairbre" )
    , ( "What is the correct name for an animal's pouch", "marsupium" )
    , ( "Baby Names Beginning With 'F': Meaning: A Field", "fell" )
    , ( "In 1965 Beatles release '---------- .'", "yesterday" )
    , ( "What country has the third most satellites in orbit", "france" )
    , ( "What is the first name of Webster, the man who published a dictionary still used today", "noah" )
    , ( "Category: History : Israel occupied the Golan Heights. Whose territory was it", "syria" )
    , ( "What is the heaviest element", "uranium" )
    , ( "Name The Year: Pres. Eisenhower signs into law National Aeronautics and Space Administration (NASA) and Space Act of 1958.", "1958" )
    , ( "What is a group of ants", "colony" )
    , ( "In what year did Franco come to power", "1937" )
    , ( "TV & Movies: 'stuart little' was a story about a _____", "mouse" )
    , ( "UnScramble this Word: o e n p e p r", "propene" )
    , ( "Aussie Slang: Dunny", "toilet" )
    , ( "Baby Names Beginning With 'A': Meaning: Vigilant, Watchful", "argus" )
    , ( "Mace is the outer covering of which common spice", "nutmeg" )
    , ( "What short lived TV western did Rod Serling produce after Twilight Zone", "loner" )
    , ( "Which country was the first to legalise abortion", "iceland" )
    , ( "Name The Year: Hernando De Soto claims the US state of Florida for Spain.", "1539" )
    , ( "Category: Geography : What is the capital of Finland", "helsinki" )
    , ( "Baby Names Beginning With 'G': Meaning: Merry, Happy", "gaye" )
    , ( "Category: Nature : This animal is the symbol of the U.S. Democratic Party.", "donkey" )
    , ( "As what was John F. Kennedy airport formerly known", "idlewild" )
    , ( "Baby Names Beginning With 'H': Meaning: Shining Brightly", "haruki" )
    , ( "Category: Chemistry : What is the main component of air", "nitrogen" )
    , ( "The 1st US minimum wage law was instituted in what year", "1938" )
    , ( "Baby Names Beginning With 'E': Meaning: Light", "elaine" )
    , ( "Category: Sport : Hockey: The Los Angeles ________.", "kings" )
    , ( "Category: Cartoon Trivia : Name Alley Oop's girl friend", "oola" )
    , ( "If it was an actual species, Furret would logically belong to which family of mammals?", "mustelidae" )
    , ( "What brand of footwear is endorsed by dr j", "converse" )
    , ( "In which year was the first artificial satellite launched", "1957" )
    , ( "Baby Names Beginning With 'C': Meaning: Little Maiden", "coralie" )
    , ( "Useless Trivia: In most---------- , including newspapers, the time displayed on a watch is 10:10.", "advertisements" )
    , ( "Baby Names Beginning With 'S': Meaning: To Stain", "sully" )
    , ( "What is the first letter of the Russian alphabet", "a" )
    , ( "UnScramble this Word: u g i r e p d", "pudgier" )
    , ( "Name The Year: Kiribati (Gilbert and Ellice Is.) gains independence from Britain.", "1979" )
    , ( "In 1986, what was the maximum fuel capacity imposed in formula 1 racing", "one" )
    , ( "This country consumes more coca cola per capita than any other.", "iceland" )
    , ( "In 1905 Felix---------- , U.S. physicist (Nobel 1952), born.", "bloch" )
    , ( "UnScramble this Word: a t i m s b p", "baptism" )
    , ( "In 1909 Victor Borge, pianist, ---------- , born.", "comedian" )
    , ( "What is the widest-ranging ocean bird", "albatross" )
    , ( "UnScramble this Word: r d g g e d a", "dragged" )
    , ( "UnScramble this Word: r b o y s l e", "soberly" )
    , ( "Where is the wailing wall", "jerusalem" )
    , ( "BaseBall - The Chicago ____", "cubs" )
    , ( "What is the largest gland in the human body", "liver" )
    , ( "TV & Movies: 1998, This Movie was Released on October 30 Living Out ----------", "loud" )
    , ( "What is the fear of rabies known as", "kynophobia" )
    , ( "Serotine, Leislers and Noctule are all varieties of which nianinial", "bat" )
    , ( "Name The Year: Rose Kennedy, Mother of a President, an Attorney General, and a Senator, born.", "1890" )
    , ( "What is the capital of connecticut", "hartford" )
    , ( "In 1906 Karl ---------- demonstrates the first 'permanent wave' for hair, in London.", "nessler" )
    , ( "Category: Trivia : What is the square root of -1", "i" )
    , ( "What part of the body has a crown, a neck & a root", "tooth" )
    , ( "TV & Movies: Lisa Loopner and Todd DiLaMuca are this", "nerds" )
    , ( "The filament of a regular light bulb is usually made of ________.", "tungsten" )
    , ( "Where is eurodisney", "paris" )
    , ( "In 1917 ---------- the Cat, cartoon character, born.", "felix" )
    , ( "Animal Trivia: Because the natural habitat of ---------- is of little use to man - the alkaline African lake waters support few fish and cannot be used for human consumption or irrigation - and also because their resting areas are typically inaccessible, the birds are rarely disturbed, unlike other African wild birds.", "flamingos" )
    , ( "Acronym Soup: Q", "queue" )
    , ( "What is the closest relative of the manatee", "elephant" )
    , ( "Squid, octopus and cuttlefish are all types of what", "cephalopods" )
    , ( "Baby Names Beginning With 'A': Meaning: Protector of Men", "alejandro" )
    , ( "'Dephlogisticated air' was the name given by Joseph Priestley to which gas", "oxygen" )
    , ( "Baby Names Beginning With 'S': Meaning: Rocky Meadow", "stanley" )
    , ( "In 1968 Borman, ---------- and Anders first men to orbit moon.", "lovell" )
    , ( "In 1898 ---------- -American War begins.", "spanish" )
    , ( "Category: Cars: Combine a Van & a Car & you get this word.", "caravan" )
    , ( "Category: Definitions : Legal Terms: A crime more serious than a misdemeanor.", "felony" )
    , ( "What was the first computer software company to go public on the New York Stock Exchange?", "cullinet" )
    , ( "TV & Movies: 1997, This Movie was Released on May 9 Father's ----------", "day" )
    , ( "Animal Trivia: ---------- can swim for a 1/2 mile without resting, and they can tread water for 3 days straight.", "rats" )
    , ( "If you were born on 27 May what star sign (Zodiac) would you be", "gemini" )
    , ( "Name The Year: Bhadwan Shree Rajneesh, indian guru, dies at 58.", "1990" )
    , ( "In which French island territory would you find the towns Bastia and Calvi", "corsica" )
    , ( "If you were born on 01 October what star sign (Zodiac) would you be", "libra" )
    , ( "If you were born on 09 April what star sign (Zodiac) would you be", "aries" )
    , ( "In 1995 Barings Bank disaster. Nick ---------- loses billions of Pounds Sterling in offshore investments, ruining Barings Bank.", "leeson" )
    , ( "TV & Movies: 1998, This Movie was Released on September 11 Without ----------", "limits" )
    , ( "Where is the fictional television station bdrx located", "bedrock" )
    , ( "As clear as a _______", "bell" )
    , ( "If you were born on 30 June what star sign (Zodiac) would you be", "cancer" )
    , ( "Useless Trivia:---------- , whales, elephants, giraffes and man all have seven neck vertebra.", "mice" )
    , ( "Where is bill gates' company based", "redmond" )
    , ( "Name The Year: Miguel Vasquez makes first public quadruple somersault on trapeze.", "1982" )
    , ( "In the USA, what is an estate agent known as", "realtor" )
    , ( "Name The Year: Chinese republic proclaimed in Tibet", "1912" )
    , ( "In 1975 Israel formally signs Sinai accord with---------- .", "egypt" )
    , ( "In 1931 ---------- Bancroft AKA Mrs Mel Brooks, Bronx, actress (Graduate), born.", "anne" )
    , ( "Baby Names Beginning With 'V': Meaning: Youthful", "verne" )
    , ( "Which animal floats in water", "porcupine" )
    , ( "Category: Books: This novel inspired the TV series 'The Six Million Dollar Man'", "cyborg" )
    , ( "In 1865 President Abraham ---------- shot in Ford's Theatre by J.W. Booth.", "lincoln" )
    , ( "UnScramble this Word: r t e d p e e", "petered" )
    , ( "UnScramble this Word: t e r e c x e", "excrete" )
    , ( "In 1942 ---------- Funichello (in Utica, NY), mouseketeer, actor, born.", "annette" )
    , ( "Atlanta 1996 Olympics: This countries medal tally was: 4 Gold, 1 Silver, 1 Bronze, 6 in Total", "turkey" )
    , ( "TV & Movies: 2000, This Movie was Released on September 22 Almost ----------", "famous" )
    , ( "Quotations: 'You can observe a lot by ------------.'- Yogi [Lawrence Peter] Berra", "watching" )
    , ( "In 1774 John Chapman, alias Johnny---------- , born.", "appleseed" )
    , ( "Unfriendly, cold and sexually unresponsive", "frigid" )
    , ( "The plant life in the oceans make up about what percent of all the greenery on the earth", "85" )
    , ( "Category: Sport : Baseball: The Toronto _________.", "bluejays" )
    , ( "TV & Movies: Twin Peaks: A burning smell is present whenever who is nearby", "bob" )
    , ( "Name The Year: Peter Ilyich Tchaikovsky, Russian composer (1812 Overture), born.", "1840" )
    , ( "In which country is the port of Stravangar", "norway" )
    , ( "Video Games: Who said 'All life begins and ends with Nu...at least this is my belief for now...'?", "nu" )
    , ( "Useless Trivia: In the summer, ---------- get a tan.", "walnuts" )
    , ( "Point Maley is the coast guard cutter in what Disney movie", "boatniks" )
    , ( "UnScramble this Word: a a j o n v", "navajo" )
    , ( "What tropic passes through Mexico", "cancer" )
    , ( "What kind of 'mate' produces a tie in a chess game", "stalemate" )
    , ( "Where did the mafia originate", "sicily" )
    , ( "Baby Names Beginning With 'A': Meaning: Wisdom", "akili" )
    , ( "UnScramble this Word: e v o m s i", "movies" )
    , ( "In what year did the Cold War begin", "1946" )
    , ( "The word 'boondocks' comes from the tagalog (filipino) word 'bundok,' which means", "mountain" )
    , ( "Name The Year: Echo I, first passive satellite launched.", "1960" )
    , ( "'Louis, 1 think this is the beginning of a beautiful friendship' are the last words of which film", "casablanca" )
    , ( "Name The Year: Mahatma Gandhi's 1st arrest, campaigning for Indian rights in S Africa.", "1914" )
    , ( "What actress played mrs margaret williams in the danny thomas show", "jean" )
    , ( "Name The Year: Brian Jones founder of the Rolling Stones, drowns.", "1969" )
    , ( "Category: Trivia : Which group of people elect the pope", "cardinals" )
    , ( "What is the most commonly spoken language in India", "hindi" )
    , ( "How many playable characters are there in \" Super Mario Kart \"?", "eight" )
    , ( "What is a pyrotechnic display", "fireworks" )
    , ( "TV & Movies: What was the shape of lolita's sunglasses in the 1962 film", "hearts" )
    , ( "TV & Movies: Category: Independent Films: David Lynch's 1976 film filled with bizarre ideas and nightmare imagery", "eraserhead" )
    , ( "Useless Trivia: In Miami, Florida, roosting vultures have taken to snatching ---------- from rooftop patios.", "poodles" )
    , ( "Category: Cars: Volvo's chairman resigned in 1993 in protest of a merger with this automaker.", "renault" )
    , ( "If you were born on 28 February what star sign (Zodiac) would you be", "pisces" )
    , ( "TV & Movies: the bunkers had neighbors who got their own series. Their last name was what", "jefferson" )
    , ( "In 1928 ---------- Mouse makes his screen debut in 'Steamboat Willie.'", "mickey" )
    , ( "TV & Movies: Category: Cartoon Sidekicks: Calvin and ------", "hobbes" )
    , ( "The most abundant metal in the earths crust is what", "aluminum" )
    , ( "What word links these: detector, polish, scrap", "metal" )
    , ( "Name the primeval supercontinent which split into Gondwanaland and Laurasia between 250 and 300 million years ago", "pangaea" )
    , ( "Useless Trivia: Apples, not---------- , are more efficient at waking up in the morning.", "caffeine" )
    , ( "What state is 'the hoosier state'", "indiana" )
    , ( "TV & Movies: 1988 - Arnold Schwarzenegger - Starred In This Movie:", "twins" )
    , ( "Baby Names Beginning With 'I': Meaning: Only Son", "iggi" )
    , ( "Category: Literature : This Shakespearean king was the actual king of Scotland for 17 years.", "macbeth" )
    , ( "Where was the septuagint written", "alexandria" )
    , ( "Which london station handles trains directly to the continent, through the channel tunnel", "waterloo" )
    , ( "TV & Movies: 1976 49th Academy Awards: Best Actress In A Leading Role Was won by Faye Dunaway For The Movie:", "network" )
    ]
