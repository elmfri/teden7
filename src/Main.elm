module Main exposing (..)

import Html exposing (..)
import Html.Events exposing (onInput, onClick)
import Random
import Questions


type alias Question =
    String


type alias Answer =
    String


type alias Model =
    { question : Question
    , answer : Answer
    , currAnswer : Answer
    , checked : Bool
    , correct : Bool
    , countCorrect : Int
    }


type Msg
    = Check
    | CurrAnsw String
    | NewQ
    | RandQA Int


model : Model
model =
    { question = ""
    , answer = ""
    , currAnswer = ""
    , checked = False
    , correct = False
    , countCorrect = 0
    }



-- view : Model -> Html Msg
-- update : Msg -> Model -> ( Model, Cmd Msg )
--main =
--    Html.program { init = ??, view = view, update = update, subscriptions = \_ -> Sub.none }
